Boston University P/N G2-HV Rev B

2 layers
0.062 in thickness
1/2 oz initial copper both layers


G2-HV-layer_2.pho      	   bottom layer (signal)
G2-HV-B_Mask.pho	   bottom solder mask
G2-HV-B_Paste.pho	   bottom solder paste
G2-HV-B_SilkS.pho	   bottom silkscreen
G2-HV.drl		   NC drill data
G2-HV-drl_map.pho	   NC drill map
G2-HV-Dwgs_User.pho	   Fabrication drawing
G2-HV-layer_1.pho	   top layer (signal)
G2-HV-F_Mask.pho	   top solder mask
G2-HV-NPTH.drl	   NC drill data - non-plated holes
G2-HV-NPTH-drl_map.pho   NC drill mp - non-plated holes

