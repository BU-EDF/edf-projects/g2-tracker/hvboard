Boston University P/N G2-HV Rev A

2 layers
0.062 in thickness
1/2 oz initial copper both layers


HVBoard-B_Cu.pho      	   bottom layer (signal)
HVBoard-B_Mask.pho	   bottom solder mask
HVBoard-B_Paste.pho	   bottom solder paste
HVBoard-B_SilkS.pho	   bottom silkscreen
HVBoard.drl		   NC drill data
HVBoard-drl_map.pho	   NC drill map
HVBoard-Dwgs_User.pho	   Fabrication drawing
HVBoard-F_Cu.pho	   top layer (signal)
HVBoard-F_Mask.pho	   top solder mask
HVBoard-NPTH.drl	   NC drill data - non-plated holes
HVBoard-NPTH-drl_map.pho   NC drill mp - non-plated holes

