update=Fri 05 Dec 2014 02:54:25 PM EST
version=1
last_client=pcbnew
[general]
version=1
[eeschema]
version=1
LibDir=lib
[eeschema/libraries]
LibName1=lib/HVBoard
LibName2=lib/power
LibName3=lib/pspice
LibName4=lib/jumper
[cvpcb]
version=1
NetIExt=net
[cvpcb/libraries]
EquName1=devcms
[pcbnew]
version=1
LastNetListRead=HVBoard.net
UseCmpFile=1
PadDrill="    2.540000"
PadDrillOvalY="    2.540000"
PadSizeH="    2.540000"
PadSizeV="    2.540000"
PcbTextSizeV="    1.500000"
PcbTextSizeH="    1.500000"
PcbTextThickness="    0.300000"
ModuleTextSizeV="    1.000000"
ModuleTextSizeH="    1.000000"
ModuleTextSizeThickness="    0.150000"
SolderMaskClearance="    0.000000"
SolderMaskMinWidth="    0.000000"
DrawSegmentWidth="    0.200000"
BoardOutlineThickness="    0.100000"
ModuleOutlineThickness="    0.150000"
[pcbnew/libraries]
LibDir=
LibName1=mod/HV_out
LibName2=mod/two_pin_jumper
LibName3=mod/C_1825
LibName4=mod/1206_resistor
LibName5=mod/HV_Jumper
LibName6=mod/HVInput
