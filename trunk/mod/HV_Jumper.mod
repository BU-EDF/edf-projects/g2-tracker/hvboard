PCBNEW-LibModule-V1  Mon 23 Feb 2015 02:14:20 PM EST
# encoding utf-8
Units mm
$INDEX
HVBoard:TH-point
$EndINDEX
$MODULE HVBoard:TH-point
Po 0 0 0 15 54EB7BD7 00000000 ~~
Li HVBoard:TH-point
Sc 0
AR /54665985
Op 0 0 0
T0 0 -1.8 1 1 0 0.15 N V 21 N "U1"
T1 0 2 1 1 0 0.15 N V 21 N "HV_jumper"
$PAD
Sh "1" C 1.8 1.8 0 0 0
Dr 1.02 0 0
At STD N 00E0FFFF
Ne 1 "N-0000034"
Po 0 0
$EndPAD
$EndMODULE HVBoard:TH-point
$EndLIBRARY
